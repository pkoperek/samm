import org.scalatra.sbt._
import org.scalatra.sbt.PluginKeys._
import com.mojolly.scalate.ScalatePlugin._
import ScalateKeys._
import com.typesafe.sbt.SbtNativePackager.autoImport._
import com.typesafe.sbt.packager.archetypes.ServerLoader
import com.typesafe.sbt.packager.docker.DockerPlugin.autoImport._
import com.typesafe.sbt.packager.docker.ExecCmd
import com.typesafe.sbt.packager.linux.LinuxPlugin.autoImport._

val ScalatraVersion = "2.5.0"

ScalatraPlugin.scalatraSettings

scalateSettings

organization := "pl.edu.agh"

name := "samm"

version := sys.env.getOrElse("DOCKER_IMAGE_VERSION", default = "0.0.1")

scalaVersion := "2.11.12"

resolvers += Classpaths.typesafeReleases

resolvers += "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases"

libraryDependencies ++= Seq(
  "org.scalatra" %% "scalatra" % ScalatraVersion,
  "org.scalatra" %% "scalatra-scalate" % ScalatraVersion,
  "org.scalatra" %% "scalatra-specs2" % ScalatraVersion % "test",
  "ch.qos.logback" % "logback-classic" % "1.1.5" % "runtime",
  "org.eclipse.jetty" % "jetty-webapp" % "9.2.15.v20160210" % "container;compile",
  "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
  "com.typesafe" % "config" % "1.3.1",
  "com.typesafe.slick" %% "slick" % "3.1.1",
  "com.h2database" % "h2" % "1.4.191",
  "io.spray" %% "spray-json" % "1.3.2",
  "com.espertech" % "esper" % "4.11.0",
  "org.jmxtrans" % "jmxtrans-core" % "263",
  "org.jmxtrans" % "jmxtrans-utils" % "263"
)

serverLoading in Debian := ServerLoader.Systemd

maintainer := "Pawel Koperek <pkoperek@gmail.com>"

packageSummary := "samm - an autonomous software management system"

packageDescription := "samm - an autonomous software management system"

dockerExposedPorts := Seq(8080)

dockerRepository := Some("registry.gitlab.com/pkoperek")

resourceGenerators in Compile <+= (resourceManaged, baseDirectory) map {
  (managedBase, base) =>
    val webappBase = base / "src" / "main" / "webapp"
    for {
      (from, to) <- webappBase ** "*" x rebase(webappBase, managedBase / "main" / "webapp")
    } yield {
      Sync.copy(from, to)
      to
    }
}

scalateTemplateConfig in Compile := {
  val base = (sourceDirectory in Compile).value
  Seq(
    TemplateConfig(
      base / "webapp" / "WEB-INF" / "templates",
      Seq.empty, /* default imports should be added here */
      Seq(
        Binding("context", "_root_.org.scalatra.scalate.ScalatraRenderContext", importMembers = true, isImplicit = true)
      ), /* add extra bindings here */
      Some("templates")
    )
  )
}

enablePlugins(JavaServerAppPackaging)

enablePlugins(JettyPlugin)

enablePlugins(DockerPlugin)

# samm #

This is a third version of SAMM tool. It differs from the predecessors in two ways:

* it is completely Scala-based
* it doesn't rely on OSGi anymore 

The original objective of SAMM is to create an autonomous software management system, which is able e.g. to automatically scale-out the application in case a spike in metrics is observed.

## Services list ##

* `/configuration/input` - configuration of input data sources
* `/configuration/rules` - configuration of Esper event processor
* `/monitored` - list of data sources
* `/measurements` - query gathered measurements (requires sending server configuration with _only_ 1 query; the
  configuration has to be the same as when used to configure the input source). The service will return all measurements
  for a given server and query pair, unless given an optional `startTimestamp` parameter, which marks the starting
  timestamp of the returned data series.
* `/events` - list of recently caught events

## Build & Run ##

```sh
$ cd samm
$ ./sbt
> jetty:start
> browse
```

If `browse` doesn't launch your browser, manually open [http://localhost:8080/](http://localhost:8080/) in your browser.

## Docker ##

Local image build and run:
```sh
$ export DOCKER_IMAGE_VERSION=3.0.1
$ ./sbt docker:publishLocal && docker-compose down && docker-compose up
```

Docker-Hub image: https://hub.docker.com/r/pkoperek/samm/

## Test environment ##

You can run the test app container together with the samm container by using:

```
$ docker-compose up
```

## Curl spells ##

Sample configuration query (works in the test env)

```sh
curl -H "Content-Type: application/json" -X POST -d @src/test/resources/example_input.json http://localhost:8080/configuration/input
```

Sample measurements query

```sh
curl -H "Content-Type: application/json" -X POST -d @src/test/resources/example_query.json http://localhost:8080/measurements
```

will return e.g.

```json
["{\"attributeName\":\"HeapMemoryUsage\",\"className\":\"sun.management.MemoryImpl\",\"objDomain\":\"java.lang\",\"typeName\":\"type=Memory\",\"values\":{\"init\":260046848,\"committed\":249561088,\"max\":3676307456,\"used\":33142376},\"epoch\":1488498628945}","{\"attributeName\":\"NonHeapMemoryUsage\",\"className\":\"sun.management.MemoryImpl\",\"objDomain\":\"java.lang\",\"typeName\":\"type=Memory\",\"values\":{\"init\":2555904,\"committed\":15400960,\"max\":-1,\"used\":14386712},\"epoch\":1488498628945}"]
```

Query with limiting timestamp:

```sh
curl -H "Content-Type: application/json" -X POST -d @src/test/resources/example_query_start.json http://localhost:8080/measurements
```

Add a sample processing rule:

```sh
curl -H "Content-Type: application/json" -X POST -d @src/test/resources/example_rule_add.json http://localhost:8080/configuration/rules
```

Return caught events (if `ruleName` is not present, all events are returned):

```sh
curl -X GET http://localhost:8080/events?ruleName=test_rule
```

Sample result:
```json
["MeasurementEvent","MeasurementEvent","MeasurementEvent"]
```
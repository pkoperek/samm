import javax.servlet.ServletContext

import org.scalatra._
import pl.edu.agh.samm._
import pl.edu.agh.samm.core.SAMMService

class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext) {
    SAMMService.init()

    context.mount(new SAMMApiServlet, "/*")
  }
}

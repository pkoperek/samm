package pl.edu.agh.samm

import org.eclipse.jetty.server.Server
import org.eclipse.jetty.servlet.DefaultServlet
import org.eclipse.jetty.webapp.WebAppContext
import org.scalatra.servlet.ScalatraListener

object JettyLauncher {

  private val portVariableName = "EXTERNAL_PORT"

  def main(args: Array[String]) {

    val port = if (System.getenv(portVariableName) != null) System.getenv(portVariableName).toInt else 8080

    val server = new Server(port)
    val context = new WebAppContext()

    val resourceBase = getClass.getClassLoader.getResource("webapp").toExternalForm
    
    context setContextPath "/"
    context.setResourceBase(resourceBase)
    context.addEventListener(new ScalatraListener)
    context.addServlet(classOf[DefaultServlet], "/")

    server.setHandler(context)
    server.start
    server.join
  }
}
package pl.edu.agh.samm

import org.slf4j.LoggerFactory
import pl.edu.agh.samm.core.SAMMService

class SAMMApiServlet extends SammStack {

  private val log = LoggerFactory.getLogger(classOf[SAMMApiServlet])

  post("/configuration/input") {
    contentType = "application/json"

    log.debug("Configuration input request: " + request.body.toString)

    SAMMService.monitorResources(request.body)
  }

  post("/configuration/rules") {
    contentType = "application/json"

    log.debug("Configuration rule request: " + request.body.toString)

    SAMMService.handleRule(request.body)
  }

  get("/monitored") {
    contentType = "application/json"

    SAMMService.listMonitored()
  }

  post("/measurements") {
    contentType = "application/json"

    SAMMService.queryMeasurements(request.body)
  }

  get("/events") {
    contentType = "application/json"

    val maybeRuleName = params.get("ruleName")

    SAMMService.events(maybeRuleName)
  }

}

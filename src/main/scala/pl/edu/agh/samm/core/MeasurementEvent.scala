package pl.edu.agh.samm.core

import com.googlecode.jmxtrans.model.{Query, Result, Server}

case class MeasurementEvent(server: Server, query: Query, result: Result)

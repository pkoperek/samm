package pl.edu.agh.samm

/**
  * Created by koperek on 01.03.17.
  */
package object core {
  val successResult =
    """
      |{
      | "result": "success"
      |}
    """.stripMargin

}

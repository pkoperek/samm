package pl.edu.agh.samm.core

import org.slf4j.LoggerFactory

class LoggingRuleHitListener extends RuleHitListener {

  private val log = LoggerFactory.getLogger(classOf[LoggingRuleHitListener])

  override def handleRuleEvent(ruleName: String, ruleEvent: RuleHitEvent): Unit = {
    log.debug("Caught event: rule name: " + ruleName + " event: " + ruleEvent)
  }
}

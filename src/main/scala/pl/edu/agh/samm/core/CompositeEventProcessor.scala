package pl.edu.agh.samm.core

class CompositeEventProcessor(
                               ruleEventProcessors: List[MeasurementEventProcessor]
                             ) extends MeasurementEventProcessor {

  override def processEvent(event: MeasurementEvent): Unit = ruleEventProcessors.foreach(p => p.processEvent(event))
}

package pl.edu.agh.samm.core

import com.googlecode.jmxtrans.model.{Query, Result, Server}
import org.slf4j.LoggerFactory

import scala.collection.mutable.ArrayBuffer

class MemoryStorageRuleHitListener(
                                    storage: scala.collection.mutable.Map[String, ArrayBuffer[RuleHitEvent]]
                                  ) extends RuleHitListener {

  override def handleRuleEvent(ruleName: String, ruleEvent: RuleHitEvent): Unit = {
    val buffer = storage.getOrElseUpdate(ruleName, ArrayBuffer[RuleHitEvent]())
    buffer.append(ruleEvent)
  }
}

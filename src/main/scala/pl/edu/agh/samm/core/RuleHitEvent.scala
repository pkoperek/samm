package pl.edu.agh.samm.core

import com.espertech.esper.client.EventBean
import spray.json.{DefaultJsonProtocol, JsArray, JsObject, JsString, JsValue, RootJsonFormat}

/**
  * Created by koperek on 11.01.17.
  */
case class RuleHitEvent(sourceEvent: EventBean)

object RuleHitEventProtocol extends DefaultJsonProtocol {

  implicit object RuleHitEventJsonFormat extends RootJsonFormat[RuleHitEvent] {
    def write(e: RuleHitEvent) = JsObject(
      "eventName" -> JsString(e.sourceEvent.getEventType.getName),
      "properties" -> JsArray()
    )

    def read(value: JsValue) =
      throw new UnsupportedOperationException
  }

}
package pl.edu.agh.samm.core

trait RuleHitListener {

  def handleRuleEvent(ruleName: String, ruleEvent: RuleHitEvent)

}

package pl.edu.agh.samm.core

import java.util.concurrent.ConcurrentHashMap

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.common.collect.ImmutableList
import com.googlecode.jmxtrans.JmxTransformer
import com.googlecode.jmxtrans.cli.JmxTransConfiguration
import com.googlecode.jmxtrans.guice.JmxTransModule
import com.googlecode.jmxtrans.model.{JmxProcess, OutputWriter, Server, _}
import com.googlecode.jmxtrans.util.PlaceholderResolverJsonNodeFactory
import org.slf4j.LoggerFactory
import spray.json.DefaultJsonProtocol._
import spray.json._

import scala.collection.JavaConversions._
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object SAMMService {

  private val log = LoggerFactory.getLogger(SAMMService.getClass.toString)

  private val measurements: scala.collection.mutable.Map[(Server, Query), ArrayBuffer[Result]] =
    mapAsScalaMap(new ConcurrentHashMap[(Server, Query), ArrayBuffer[Result]]())
  private val ruleHitEvents: scala.collection.mutable.Map[String, ArrayBuffer[RuleHitEvent]] =
    mapAsScalaMap(new ConcurrentHashMap[String, ArrayBuffer[RuleHitEvent]])

  private val ruleEventListeners = List(new LoggingRuleHitListener, new MemoryStorageRuleHitListener(ruleHitEvents))
  private val ruleEventProcessor = new EsperMeasurementEventProcessor(ruleEventListeners)
  private val storageEventProcessor = new MemoryStorageMeasurementEventProcessor(measurements)
  private val outputWriter = new SAMMOutputWriter(new CompositeEventProcessor(List(ruleEventProcessor, storageEventProcessor)))
  private val injector = JmxTransModule.createInjector(new JmxTransConfiguration())
  private val objectMapper = injector.getInstance(classOf[ObjectMapper])
  private val placeholderResolverJsonNodeFactory = injector.getInstance(classOf[PlaceholderResolverJsonNodeFactory])
  private val transformer = injector.getInstance(classOf[JmxTransformer])
  private val sammOutputWriter = asJavaCollection(List[OutputWriter](outputWriter))
  private val monitored: mutable.Map[String, JmxProcess] = mapAsScalaMap(new ConcurrentHashMap[String, JmxProcess]())

  def monitorResources(body: String): Unit = {
    def extendServer(s: Server): Server = {
      Server.builder(s).addOutputWriters(sammOutputWriter).build()
    }

    def guavaAsScalaCollection(list: ImmutableList[Server]): Array[Server] = {
      list.toArray(Array[Server]())
    }

    def scalaCollectionAsGuava(servers: Seq[Server]): ImmutableList[Server] = {
      ImmutableList.copyOf(asJavaIterable(servers))
    }

    val jmxProcess = parseProcess(body)
    val servers = guavaAsScalaCollection(jmxProcess.getServers)
    val extendedServers = servers.map(s => extendServer(s))
    val serversAsGuavaCollection = scalaCollectionAsGuava(extendedServers)
    val updatedProcess = new JmxProcess(serversAsGuavaCollection)
    updatedProcess.setName(jmxProcess.getName)

    scheduleMonitoring(updatedProcess)
  }

  private def parseProcess(json: String): JmxProcess = {
    val jsonNode = objectMapper.readTree(json)
    val jmx = objectMapper.treeToValue(jsonNode, classOf[JmxProcess])
    jmx
  }

  private def parseServer(json: String): Server = {
    val jsonNode = objectMapper.readTree(json)
    val server = objectMapper.treeToValue(jsonNode, classOf[Server])
    server
  }

  private def scheduleMonitoring(updatedProcess: JmxProcess): String = {
    transformer.executeStandalone(updatedProcess)

    log.debug("Process: " + updatedProcess.getName)

    monitored.put(updatedProcess.getName, updatedProcess)

    successResult
  }

  def listMonitored(): String = {
    monitored.toMap.map(pair => pair._1 -> objectMapper.writeValueAsString(pair._2)).toJson.toString
  }

  def init(): Unit = {
    log.debug("SAMMService initialized")

    objectMapper.setNodeFactory(placeholderResolverJsonNodeFactory)
  }

  def handleRule(ruleRequestAsString: String): String = {
    val ruleRequestAsJson = ruleRequestAsString.parseJson

    def stripQuotes(input: String): String = {
      input.trim.stripPrefix("\"").stripSuffix("\"")
    }

    val action = stripQuotes(extractJSONField(ruleRequestAsJson, "action"))
    val name = stripQuotes(extractJSONField(ruleRequestAsJson, "name"))
    val statement = stripQuotes(extractJSONField(ruleRequestAsJson, "statement"))

    log.debug("Handling: action: " + action + " rule name: " + name + " statement: " + statement)

    if ("add".equals(action)) {
      ruleEventProcessor.addRule(name, statement)
    } else {
      ruleEventProcessor.removeRule(name)
    }

    successResult
  }

  def events(ruleName: Option[String]): String = {
    val eventsToReturn = if (ruleName.isDefined) {
      ruleHitEvents.getOrElse(ruleName.get, ArrayBuffer[RuleHitEvent]()).toList
    } else {
      ruleHitEvents.map(pair => pair._2.toList).toList.flatten
    }

    eventsToReturn.map(e => e.sourceEvent.getEventType.getName).toJson.toString()
  }

  def queryMeasurements(requestAsString: String): String = {
    val requestAsJson = requestAsString.parseJson

    val timestamp = extractJSONFieldOptional(requestAsJson, "startTimestamp").getOrElse("0").toLong
    val server = parseServer(extractJSONField(requestAsJson, "server"))

    val key = (server, server.getQueries().asList().head)
    val storedMeasurements = measurements.getOrElse(key, ArrayBuffer[Result]())

    storedMeasurements
      .filter(r => r.getEpoch >= timestamp)
      .map(r => objectMapper.writeValueAsString(r))
      .toList
      .toJson
      .toString
  }

  private def extractJSONField(requestAsJson: JsValue, field: String): String = {
    requestAsJson.asJsObject.fields(field).toString()
  }

  private def extractJSONFieldOptional(requestAsJson: JsValue, field: String): Option[String] = {
    val retrieved = requestAsJson.asJsObject.fields.get(field)

    if (retrieved.isDefined) {
      Some(retrieved.get.toString())
    } else {
      None
    }
  }

}

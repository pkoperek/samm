package pl.edu.agh.samm.core

abstract class MeasurementEventProcessor {

  def processEvent(event: MeasurementEvent): Unit

}

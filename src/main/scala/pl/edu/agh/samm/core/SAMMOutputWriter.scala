package pl.edu.agh.samm.core

import java.lang.Iterable
import java.util

import com.googlecode.jmxtrans.model.{OutputWriter, Query, Result, Server}

import scala.collection.JavaConversions._

class SAMMOutputWriter(ruleEventProcessor: MeasurementEventProcessor) extends OutputWriter {

  override def validateSetup(server: Server, query: Query): Unit = {
    // no op
  }

  override def start(): Unit = {}

  override def close(): Unit = {}

  override def getSettings: util.Map[String, AnyRef] = emptyMap

  private def emptyMap: util.Map[String, AnyRef] = mapAsJavaMap(Map[String, AnyRef]())

  override def doWrite(server: Server, query: Query, results: Iterable[Result]): Unit = {
    results.foreach(
      result => ruleEventProcessor.processEvent(MeasurementEvent(server, query, result))
    )
  }

}

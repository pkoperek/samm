package pl.edu.agh.samm.core

import com.googlecode.jmxtrans.model.{Query, Result, Server}

import scala.collection.mutable.ArrayBuffer

class MemoryStorageMeasurementEventProcessor(
                             storage: scala.collection.mutable.Map[(Server, Query), ArrayBuffer[Result]]
                           ) extends MeasurementEventProcessor {

  override def processEvent(event: MeasurementEvent): Unit = {
    val key = (event.server, event.query)
    val value = event.result
    val results = storage.getOrElseUpdate(key, ArrayBuffer[Result]())
    results.append(value)
  }
}

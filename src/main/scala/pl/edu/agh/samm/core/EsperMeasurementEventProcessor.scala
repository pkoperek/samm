package pl.edu.agh.samm.core

import com.espertech.esper.client.{Configuration, EPStatement, EventBean, UpdateListener}
import org.slf4j.LoggerFactory

class EsperMeasurementEventProcessor(ruleEventListeners: List[RuleHitListener]) extends MeasurementEventProcessor {

  private val log = LoggerFactory.getLogger(classOf[EsperMeasurementEventProcessor])
  private val configuration: Configuration = createConfiguration()
  private val esperProvider = com.espertech.esper.client.EPServiceProviderManager.getDefaultProvider(configuration)
  private val esperRuntime = esperProvider.getEPRuntime
  private val esperAdministrator = esperProvider.getEPAdministrator

  private def createConfiguration() = {
    val configuration = new Configuration
    configuration.addEventType(classOf[MeasurementEvent])
    configuration
  }

  override def processEvent(event: MeasurementEvent): Unit = {
    log.debug("Processing event: " + event)

    esperRuntime.sendEvent(event)
  }

  def addRule(ruleName: String, statementAsString: String): Unit = {

    val statement: EPStatement = esperAdministrator.createEPL(statementAsString, ruleName)

    val updateListener = new UpdateListener {
      override def update(newEvents: Array[EventBean], oldEvents: Array[EventBean]): Unit = {

        // we are not interested in cases when events are removed i.e. oldEvents != null
        if (newEvents != null) {
          ruleEventListeners.foreach(
            eventListener =>
              newEvents.foreach(event =>
                eventListener.handleRuleEvent(ruleName, new RuleHitEvent(event))
              )
          )
        }
      }
    }

    statement.addListener(updateListener)
  }

  def removeRule(ruleName: String): Unit = {
    val statement: EPStatement = esperAdministrator.getStatement(ruleName)
    statement.destroy()
  }
}

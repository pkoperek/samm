# 3.0.2

* Limiting the number of data points returned by `/measurements`
  with the `startTimestamp` field.

# 3.0.1

* Integration of Esper
* Split of configuration service into `input` and `rules`
* `events` endpoint which will allow monitoring caught/generated events

# 3.0.0

* Accesing JVMs with JmxTrans
* `configuration` service
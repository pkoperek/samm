Building
========

```
$ docker build . -t registry.gitlab.com/pkoperek/samm-testapp:1.0.0
```

Deploying to dockerhub
======================

```
$ docker login
$ docker tag registry.gitlab.com/pkoperek/samm-testapp:1.0.0 pkoperek/samm-testapp:latest
$ docker push pkoperek/samm-testapp
```
